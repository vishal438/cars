const inventory = require('../inventory.cjs');
const problem1 = require('../problem1.cjs');

const result=problem1(inventory,0)

if(!Array.isArray(result) && Object.keys(result).length!==0){
    let ans=`car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`
    console.log(ans)
}
else{
    console.log(result)
}
